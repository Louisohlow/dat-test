package com.sevensenders.response

import com.sevensenders.response.Response
import io.restassured.response.ValidatableResponse

class ConsignmentResponse extends Response{
    ConsignmentResponse(ValidatableResponse validatableResponse){
        super(validatableResponse)
    }
}
