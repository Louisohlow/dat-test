package com.sevensenders.api.basic

import com.sevensenders.app.Endpoints
import com.sevensenders.utils.Configuration
import io.restassured.RestAssured
import io.restassured.response.ValidatableResponse
import org.json.JSONObject

class Post {

    static ValidatableResponse makeRequest(String jwtToken, String url, String body){
        return RestAssured.given()
                .header("Authorization", "Bearer " + jwtToken)
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .when()
                .body(body)
                .post(url)
                .then().log().all()
    }

    // getting the JWT authentication token for authorization
    static String token() {
        JSONObject body = new JSONObject()
                .put("username", Configuration.getUN())
                .put("password", Configuration.getPW())

        def request = RestAssured.given()
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .body(body.toString())
                .when()
                .post(Endpoints.TOKEN).jsonPath().get("token").toString()

        return request
    }

}
