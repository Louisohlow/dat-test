package com.sevensenders.api.basic

import io.restassured.RestAssured
import io.restassured.response.ValidatableResponse

class Get {

    static ValidatableResponse makeRequest(String jwtToken, String url){
        RestAssured.given()
                .header("Authorization", "Bearer " + jwtToken)
                .header("accept", "application/json")
                .when()
                .get(url)
                .then()
    }

    static ValidatableResponse makeRequestwParam(String jwtToken, String url, String firstParam, String firstParamVal, String secondParam, String secondParamVal ){
        return RestAssured.given()
                .header("Authorization", "Bearer " + jwtToken)
                .header("Content-Type", "application/json")
                .queryParam(firstParam, firstParamVal)
                .queryParam(secondParam, secondParamVal)
                .when()
                .get(url)
                .then()
    }
}
