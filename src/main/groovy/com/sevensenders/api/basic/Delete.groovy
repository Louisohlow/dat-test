package com.sevensenders.api.basic

import com.sevensenders.utils.Configuration
import io.restassured.RestAssured
import io.restassured.response.ValidatableResponse

class Delete {

    static ValidatableResponse makeRequest(String jwtToken, String url){
        return RestAssured.given()
                .header("Authorization", "Bearer " + jwtToken)
                .header("accept", "application/json")
                .when()
                .delete(url)
                .then()
    }

    // basic example of Post request
    static def request(String url, String apikey = Configuration.getApiKey(), int id) {
        return RestAssured.given()
                .queryParam("id", id)
                .when()
                .delete(url)
    }
}
