package com.sevensenders.utils

import static com.sevensenders.utils.PropertyReader.getKey

class Configuration {
    static getEnvironment() {
        getKey("environment")
    }

    static String getPW() {
        getKey("password", getEnvironment())
    }

    static String getUN() {
        getKey("username", getEnvironment())
    }

    static getDatApiURL() {
        getKey("apiUrl", getEnvironment())
    }
}
