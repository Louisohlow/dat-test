package dat.consignments

import com.sevensenders.api.basic.Post
import com.sevensenders.api.consignments.ConsignmentGetRequest
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

class GetConsignmentsTest {
    String token

    @BeforeClass
    void before(){
        token = Post.token()
    }

    @Test
    void getAllConsignments(){
        ConsignmentGetRequest.getConsignmentwParams(token, "direction", "outbound", "plannedPickUpTime[after]", "2018-07-01T22:00:00.000Z")
                .assertStatusCode(200)
    }
}
