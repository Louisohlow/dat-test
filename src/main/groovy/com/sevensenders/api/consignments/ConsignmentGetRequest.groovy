package com.sevensenders.api.consignments

import com.sevensenders.api.basic.Get
import com.sevensenders.app.Endpoints
import com.sevensenders.response.ConsignmentResponse

class ConsignmentGetRequest {
    static def postLabelBy(String token){
        def res = new ConsignmentResponse((Get.makeRequest(token, Endpoints.CONSIGNMENT + "/create")))
        return res
    }

    static def getConsignmentwParams(String token, String firstParam, String firstParamVal, String secondParam, String secondParamVal){
        def res = new ConsignmentResponse((Get.makeRequestwParam(token, Endpoints.CONSIGNMENT, firstParam, firstParamVal, secondParam, secondParamVal)))
        return res
    }
}
