// environment variables
apiKey = System.getenv("access_key") ?: "eyJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Im9wZXJhdGlvbnMrYXBpU2V2ZW5TZW5kZXJzMTUzNDk0NjYwNUBzZXZlbnNlbmRlcnMuY29tIiwidXNlcm5hbWUiOiJhcGlTZXZlblNlbmRlcnMxNTM0OTQ2NjA1IiwicGFzc3dvcmQiOiIxYk99XXF3RHlAOHleOVhYXmE2U3J2JlYzN2IkflByKiIsImlhdCI6MTUzNDk0NjYwNSwiaXNzIjoiU2V2ZW4gU2VuZGVycyIsImF1ZCI6IlNpbmdsZSBBUEkifQ.wyJubk9c7b0WG4wH0kX5_wYQjgmH9oqDsPjSiuJ188U"
environment = System.getenv("TEST_ENVIRONMENT") ?: "test"

// different environments that have specific environment variables
environments {
    test{
        apiUrl = "https://dat-test.network.sevensenders.com/api"
        password = "QA_test_2017"
        username = "FF2_test"
    }
}