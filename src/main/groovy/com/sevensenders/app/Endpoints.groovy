package com.sevensenders.app

import com.sevensenders.utils.Configuration

class Endpoints {
    private static String API_URL = Configuration.getDatApiURL()

    public static final String TOKEN = API_URL + "/token"
    public static final String CONSIGNMENT = API_URL + "/consignments"

}
